﻿using System.Collections.Generic;

namespace LinkedInGrabber
{
    public class Human
    {
        public string Link;
        public string Name;
        public List<WorkExp> Work = new List<WorkExp>();
        public List<EducationExp> Education = new List<EducationExp>();
    }

    public class WorkExp
    {
        public string JobTitle;
        public string Conpany;
        public string StartDate;
        public string EndDate;
    }

    public class EducationExp
    {
        public string School;
        public string Degree;
        public string StartDate;
        public string EndDate; 
    }
}
