﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using FileSystem;

namespace LinkedInGrabber
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (args.Length != 0)
            {
                if (args[0] == "/config")
                {
                    Application.Run(new GrabberConfigurator());
                }
            }
            else
            {
                if (FS.IsAdministrator())
                {
                    FS.CleanUpSeleniumTmpFiles();

                    Application.Run(new Form1());
                }
            }
        }
    }
}
