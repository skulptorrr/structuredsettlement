﻿namespace LinkedInGrabber
{
    partial class GrabberConfigurator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.Delay = new System.Windows.Forms.NumericUpDown();
            this.Save = new System.Windows.Forms.Button();
            this.FileName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.RezPath = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.fjs = new System.Windows.Forms.RadioButton();
            this.chrome = new System.Windows.Forms.RadioButton();
            this.ff = new System.Windows.Forms.RadioButton();
            this.Url = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.LoginFld = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.PasswordFld = new System.Windows.Forms.TextBox();
            this.googleProfilesCheckBox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.Delay)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(277, 209);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "Delay in seconds";
            // 
            // Delay
            // 
            this.Delay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Delay.Location = new System.Drawing.Point(371, 207);
            this.Delay.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.Delay.Name = "Delay";
            this.Delay.Size = new System.Drawing.Size(56, 20);
            this.Delay.TabIndex = 24;
            // 
            // Save
            // 
            this.Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Save.Location = new System.Drawing.Point(449, 204);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(110, 23);
            this.Save.TabIndex = 23;
            this.Save.Text = "Save";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // FileName
            // 
            this.FileName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.FileName.Location = new System.Drawing.Point(83, 206);
            this.FileName.Name = "FileName";
            this.FileName.Size = new System.Drawing.Size(185, 20);
            this.FileName.TabIndex = 22;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 206);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "FileName";
            // 
            // RezPath
            // 
            this.RezPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RezPath.Location = new System.Drawing.Point(103, 177);
            this.RezPath.Name = "RezPath";
            this.RezPath.Size = new System.Drawing.Size(456, 20);
            this.RezPath.TabIndex = 20;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 180);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Results Folder";
            // 
            // fjs
            // 
            this.fjs.AutoSize = true;
            this.fjs.Location = new System.Drawing.Point(153, 79);
            this.fjs.Name = "fjs";
            this.fjs.Size = new System.Drawing.Size(107, 17);
            this.fjs.TabIndex = 18;
            this.fjs.Text = "headless browser";
            this.fjs.UseVisualStyleBackColor = true;
            // 
            // chrome
            // 
            this.chrome.AutoSize = true;
            this.chrome.Enabled = false;
            this.chrome.Location = new System.Drawing.Point(87, 79);
            this.chrome.Name = "chrome";
            this.chrome.Size = new System.Drawing.Size(60, 17);
            this.chrome.TabIndex = 17;
            this.chrome.Text = "chrome";
            this.chrome.UseVisualStyleBackColor = true;
            // 
            // ff
            // 
            this.ff.AutoSize = true;
            this.ff.Checked = true;
            this.ff.Location = new System.Drawing.Point(28, 79);
            this.ff.Name = "ff";
            this.ff.Size = new System.Drawing.Size(53, 17);
            this.ff.TabIndex = 16;
            this.ff.TabStop = true;
            this.ff.Text = "firefox";
            this.ff.UseVisualStyleBackColor = true;
            // 
            // Url
            // 
            this.Url.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Url.Location = new System.Drawing.Point(61, 102);
            this.Url.Name = "Url";
            this.Url.Size = new System.Drawing.Size(503, 20);
            this.Url.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 105);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "URL";
            // 
            // LoginFld
            // 
            this.LoginFld.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LoginFld.Location = new System.Drawing.Point(79, 12);
            this.LoginFld.Name = "LoginFld";
            this.LoginFld.Size = new System.Drawing.Size(129, 20);
            this.LoginFld.TabIndex = 28;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 29;
            this.label6.Text = "Login";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 31;
            this.label7.Text = "Password";
            // 
            // PasswordFld
            // 
            this.PasswordFld.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PasswordFld.Location = new System.Drawing.Point(79, 38);
            this.PasswordFld.Name = "PasswordFld";
            this.PasswordFld.Size = new System.Drawing.Size(129, 20);
            this.PasswordFld.TabIndex = 30;
            // 
            // googleProfilesCheckBox
            // 
            this.googleProfilesCheckBox.AutoSize = true;
            this.googleProfilesCheckBox.Location = new System.Drawing.Point(389, 15);
            this.googleProfilesCheckBox.Name = "googleProfilesCheckBox";
            this.googleProfilesCheckBox.Size = new System.Drawing.Size(143, 17);
            this.googleProfilesCheckBox.TabIndex = 32;
            this.googleProfilesCheckBox.Text = "Google profiles if needed";
            this.googleProfilesCheckBox.UseVisualStyleBackColor = true;
            // 
            // GrabberConfigurator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(582, 248);
            this.Controls.Add(this.googleProfilesCheckBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.PasswordFld);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.LoginFld);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Delay);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.FileName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.RezPath);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.fjs);
            this.Controls.Add(this.chrome);
            this.Controls.Add(this.ff);
            this.Controls.Add(this.Url);
            this.Controls.Add(this.label1);
            this.Name = "GrabberConfigurator";
            this.Text = "GrabberConfigurator";
            this.Load += new System.EventHandler(this.GrabberConfigurator_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Delay)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown Delay;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.TextBox FileName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox RezPath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton fjs;
        private System.Windows.Forms.RadioButton chrome;
        private System.Windows.Forms.RadioButton ff;
        private System.Windows.Forms.TextBox Url;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox LoginFld;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox PasswordFld;
        private System.Windows.Forms.CheckBox googleProfilesCheckBox;

    }
}