﻿using System.Threading;
using BotAgent.Ifrit.Core;
using BotAgent.Ifrit.Core.BaseClasses;

namespace LinkedInGrabber
{
    public static class GoogleSearcher
    {
        public static void GoogleProfile(IfrBrowser brwsr, Human profile)
        {
            string strToSearch = BuildStr(profile);

            GoogleByPhrase(brwsr, strToSearch);
            CheckPageForLinkedInLink(brwsr);
        }

        private static string BuildStr(Human profile)
        {
            var rez = string.Format("{0} {1} {2}", profile.Education[0], profile.Work[0], profile.Work[1]);

            return rez;
        }

        private static void GoogleByPhrase(IfrBrowser brwsr, string strToSearch)
        {
            brwsr.Nav.GoTo("https://www.google.com");
            brwsr.Page.Elem(By.Id("lst-ib")).AsInput().SetTextAndSubmit(strToSearch);

            Thread.Sleep(500);
        }

        private static void CheckPageForLinkedInLink(IfrBrowser brwsr)
        {
            var links = brwsr.Page.Elems(By.Xpath("id('rso')/div/div/div/h3/a")).AsList();

            foreach (var lnk in links)
            {
                if (lnk.AsLink().Href.Contains("www.linkedin.com"))
                {
                    lnk.AsLink().ClickAndWaitForPageLoad();
                    return;
                }
            }
        }
    }
}
