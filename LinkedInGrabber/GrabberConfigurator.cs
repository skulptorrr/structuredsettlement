﻿using System;
using System.Windows.Forms;
using LinkedInGrabber.Properties;

namespace LinkedInGrabber
{
    public partial class GrabberConfigurator : Form
    {
        public GrabberConfigurator()
        {
            InitializeComponent();
        }

        private void Save_Click(object sender, EventArgs e)
        {
            if (ff.Checked)
            {
                Settings.Default.Browser = "ff";
            }
            else if (chrome.Checked == true)
            {
                Settings.Default.Browser = "chrome";
            }
            else if (fjs.Checked == true)
            {
                Settings.Default.Browser = "fjs";
            }

            Settings.Default.PathToSaveRezults = RezPath.Text;

            Settings.Default.Url = Url.Text;

            Settings.Default.RezultFileName = FileName.Text;

            if ((int) Delay.Value > 0)
            {
                Settings.Default.WaitPeriod = (int)Delay.Value * 1000;
            }

            Settings.Default.Login = LoginFld.Text;
            Settings.Default.Pass = PasswordFld.Text;

            Settings.Default.GooglingEnabled = googleProfilesCheckBox.Checked;

            Settings.Default.Save();

            this.Close();
        }

        private void GrabberConfigurator_Load(object sender, EventArgs e)
        {
            switch (Settings.Default.Browser)
            {
                case ("ff"):
                    ff.Checked = true;
                    break;
                case ("chrome"):
                    chrome.Checked = true;
                    break;
                case ("fjs"):
                    fjs.Checked = true;
                    break;
            }

            googleProfilesCheckBox.Checked = Settings.Default.GooglingEnabled;

            RezPath.Text = Settings.Default.PathToSaveRezults;

            Url.Text = Settings.Default.Url;

            FileName.Text = Settings.Default.RezultFileName;

            if ((int)Delay.Value > 0)
            {
                Settings.Default.WaitPeriod = (int)Delay.Value / 1000;
            }
            
            LoginFld.Text = Settings.Default.Login;
            PasswordFld.Text = Settings.Default.Pass;
        }
    }
}
