﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using BotAgent.Ifrit.Core;
using BotAgent.Ifrit.Core.BaseClasses;
using BotAgent.DataExporter;
using LinkedInGrabber.Properties;

namespace LinkedInGrabber
{
    public partial class Form1 : Form
    {
        private static Csv _csvFile = new Csv();
        private Thread _workerThread;
        private IfrBrowser Brwsr;
        
        private bool _runningState = true;

        private string[] profilesLinks = new string[10];
        private string currentSearchPage = "";

        private List<Human> _collectedProfiles = new List<Human>();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _workerThread = new Thread(DoWork);
            _workerThread.Start();
        }

        private void DoWork()
        {
            StartWork();
            do
            {
                UpdateProfilesLinks();

                foreach (var profileLink in profilesLinks)
                {
                    if (profileLink == null)
                    {
                        MessageBox.Show("For some reason profile link is null. Possible causes: 1. Search finished. 2 some mistakes in code. 3. There are no profiles links on the page/ this is not search page");
                        break;
                    }

                    Brwsr.Nav.GoToAndWaitForPageLoad(profileLink);
                    ParseInfoFromProfile();
                    GoogleLastProfileIfNeeded();
                }

                RebuildCsvVariable();
                SaveCsvToHdd();
            } 
            while (currentSearchPage != String.Empty);
            
            EndWork();
        }

        private void SaveCsvToHdd()
        {
            try
            {
                _csvFile.FileSave(Settings.Default.PathToSaveRezults.TrimEnd('\\') + "\\" + Settings.Default.RezultFileName + ".csv");
            }
            catch (Exception)
            {
                Brwsr.Close();
                throw new Exception("Failed to save CSV file. Looks like you forget to run program as Administrator or set incorrect filepath.");
            }
        }

        private void StartWork()
        {
            //// HideMainForm
            //this.WindowState = FormWindowState.Minimized;
            //this.ShowInTaskbar = false;

            Brwsr = new IfrBrowser();

            currentSearchPage = Settings.Default.Url;

            this.button1.Enabled = true;

            PerformLoginToLinkedIn();
        }
        
        private void EndWork()
        {
            //SaveCsvToHdd();

            Brwsr.Close();

            this.Invoke(new Action(() => Close()));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (_runningState)
            {
                _workerThread.Suspend();

                RebuildCsvVariable();
                SaveCsvToHdd();

                button1.Text = "Resume";
                _runningState = false;
            }
            else
            {
                _workerThread.Resume();
                button1.Text = "Pause";
                _runningState = true;
            }
        }
        
        private void PerformLoginToLinkedIn()
        {
            Brwsr.Nav.GoToAndWaitForPageLoad("https://www.linkedin.com");
            Brwsr.Page.Elem(By.Id("login-email")).AsInput().Text = Settings.Default.Login;
            Brwsr.Page.Elem(By.Id("login-password")).AsInput().SetTextAndSubmit(Settings.Default.Pass);

            Thread.Sleep(5000);
        }

        private void UpdateProfilesLinks()
        {
            Array.Clear(profilesLinks, 0, profilesLinks.Length);
            
            if(currentSearchPage!= string.Empty)
            {
                Brwsr.Nav.GoToAndWaitForPageLoad(currentSearchPage);

                var profilesLinksElems = Brwsr.Page.Elems(By.Xpath("id('results')/li/div/h3/a")).AsList();

                if (Brwsr.Page.PageSource.Contains("you’ve reached the commercial use limit on search"))
                {
                    MessageBox.Show("You’ve reached the commercial use limit on search. Cannot crawl more! Program will be automatically paused");
                    button1_Click(null, null);
                }

                    for (int i = 0; i < 10; i++)
                    {
                        profilesLinks[i] = profilesLinksElems[i].AsLink().Href;

                        var nextBtn = Brwsr.Page.Elem(By.Xpath(".//a[@rel='next']")).AsLink();

                        currentSearchPage = nextBtn.Href;
                    }

                Thread.Sleep(Settings.Default.WaitPeriod);
            }
        }

        private void ParseInfoFromProfile()
        {
            if (Brwsr.Page.Url.Contains("www.linkedin.com/profile/view?id="))
            {
                _collectedProfiles.Add(new Human());

                _collectedProfiles.Last().Name = Brwsr.Page.Elem(By.Xpath("id('name')/h1/span/span")).AsOther().Text; ;

                _collectedProfiles.Last().Link = Brwsr.Page.Url;

                int worksCounter = Brwsr.Page.Elems(By.Xpath("id('background-experience')/div")).Count;

                int educationCounter = Brwsr.Page.Elems(By.Xpath("id('background-education')/div")).Count;

                string tmpSchool;
                string tmpDegree;
                string tmpJobTitle;
                string tmpCompany;
                string tmpStartDate;
                string tmpEndDate;

                if (worksCounter >= 1)
                {
                    for (int i = 1; i <= worksCounter; i++)
                    {
                        tmpJobTitle = Brwsr.Page.Elem(By.Xpath("id('background-experience-container')/div/div[" + i + "]/div/header/h4"), Await.Present(500)).AsOther().Text;

                        tmpCompany = Brwsr.Page.Elem(By.Xpath("id('background-experience-container')/div/div[" + i + "]/div/header/h5[2]"), Await.Present(500)).AsOther().TextOfTagsTree;
                        if (tmpCompany == String.Empty)
                        {
                            tmpCompany = Brwsr.Page.Elem(By.Xpath("id('background-experience-container')/div/div[" + i + "]/div/header/h5[1]"), Await.Present(500)).AsOther().TextOfTagsTree;
                        }

                        tmpStartDate = Brwsr.Page.Elem(By.Xpath("id('background-experience-container')/div/div[" + i + "]/div/span/time[1]"), Await.Present(500)).AsOther().Text;

                        tmpEndDate = Brwsr.Page.Elem(By.Xpath("id('background-experience-container')/div/div[" + i + "]/div/span/time[2]"), Await.Present(500)).AsOther().Text;
                        if (tmpEndDate == String.Empty)
                        {
                            tmpEndDate = "Present";
                        }

                        _collectedProfiles.Last().Work.Add(new WorkExp { JobTitle = tmpJobTitle, Conpany = tmpCompany, EndDate = tmpEndDate, StartDate = tmpStartDate });
                    }
                }
                else
                {
                    tmpJobTitle = Brwsr.Page.Elem(By.Xpath("id('headline')/p"), Await.Present(500)).AsOther().Text;
                    tmpCompany = Brwsr.Page.Elem(By.Xpath("id('overview-summary-current')/td/ol/li/span/strong/a"), Await.Present(500)).AsOther().Text;

                    _collectedProfiles.Last().Work.Add(new WorkExp { JobTitle = tmpJobTitle, Conpany = tmpCompany });

                    var otherWorks = Brwsr.Page.Elems(By.Xpath("id('overview-summary-past')/td/ol/li/span/strong/a")).AsList();

                    foreach (var work in otherWorks)
                    {
                        tmpCompany = work.AsOther().Text;
                        _collectedProfiles.Last().Work.Add(new WorkExp { Conpany = tmpCompany });
                    }
                }

                if (educationCounter >= 1)
                {
                    for (int i = 1; i <= educationCounter; i++)
                    {
                        tmpSchool = Brwsr.Page.Elem(By.Xpath("id('background-education-container')/div/div[" + i + "]/div/div/header/h4")).AsOther().Text;
                        tmpDegree = Brwsr.Page.Elem(By.Xpath("id('background-education-container')/div/div[" + i + "]/div/div/header/h5")).AsOther().TextOfTagsTree;
                        tmpStartDate = Brwsr.Page.Elem(By.Xpath("id(background-education-container')/div/div[" + i + "]/div/div/span/time[1]")).AsOther().Text;
                        tmpEndDate = Brwsr.Page.Elem(By.Xpath("id('background-education-container')/div/div[" + i + "]/div/div/span/time[2]")).AsOther().Text;

                        _collectedProfiles.Last().Education.Add(new EducationExp { School = tmpSchool, Degree = tmpDegree, EndDate = tmpEndDate, StartDate = tmpStartDate });
                    }
                }
                else
                {
                    tmpSchool = Brwsr.Page.Elem(By.Xpath("id('overview-summary-education')/td/ol/li/a")).AsOther().Text;
                    _collectedProfiles.Last().Education.Add(new EducationExp { School = tmpSchool });
                }

                Thread.Sleep(Settings.Default.WaitPeriod);
            }
        }

        private void GoogleLastProfileIfNeeded()
        {
            if (Settings.Default.GooglingEnabled)
            {
                Human profile = _collectedProfiles.Last();

                if (profile.Name.Contains("Member"))
                {
                    GoogleSearcher.GoogleProfile(Brwsr, profile);
                }

                ParseInfoFromProfile();
            }
        }

        private void RebuildCsvVariable()
        {
            int maxOfWorksPerProfile = 0;
            int maxOfEducationsPerProfiles = 0;
            int firstEducationCellNumber;

            foreach (var profile in _collectedProfiles)
            {
                if (profile.Education.Count > maxOfEducationsPerProfiles)
                    maxOfEducationsPerProfiles = profile.Education.Count;

                if (profile.Work.Count > maxOfWorksPerProfile)
                    maxOfWorksPerProfile = profile.Work.Count;
            }

            firstEducationCellNumber = 2 + maxOfWorksPerProfile * 4;

            int cells = firstEducationCellNumber+ maxOfEducationsPerProfiles*4;

            RebuildCsvHeaderString(firstEducationCellNumber, cells);

            foreach (var human in _collectedProfiles)
            {
                AddHumanToCsv(human, firstEducationCellNumber, cells);
            }
        }
        
        private void RebuildCsvHeaderString(int firstEducationCellNumber, int cells)
        {
            _csvFile.Rows.Clear();

            string[] row = new string[cells];

            row[0] = "Name";
            row[1] = "Link";
            var circle = 1;

            for (int currCellNumber = 2; currCellNumber < firstEducationCellNumber; currCellNumber += 4)
            {
                row[currCellNumber] = "Job Title " + circle;
                row[currCellNumber + 1] = "Company " + circle;
                row[currCellNumber + 2] = "Start Date " + circle;
                row[currCellNumber + 3] = "End Date " + circle;

                circle++;
            }

            circle = 1;

            for (int currCellNumber = firstEducationCellNumber; currCellNumber < cells; currCellNumber += 4)
            {
                row[currCellNumber] = "School " + circle;
                row[currCellNumber + 1] = "Degree " + circle;
                row[currCellNumber + 2] = "Start Date " + circle;
                row[currCellNumber + 3] = "End Date " + circle;

                circle++;
            }

            _csvFile.Rows.Add(row.ToList());
        }

        private void AddHumanToCsv(Human human, int firstEducationCellNumber, int cells)
        {
            string[] row = new string[cells];

            row[0] = human.Name;
            row[1] = human.Link;

            var circle = 0;
            var maxWorkCell = 2 + human.Work.Count * 4;
            var maxEduCell = firstEducationCellNumber + human.Education.Count * 4;


            for (int currCellNumber = 2; currCellNumber < maxWorkCell; currCellNumber += 4)
            {
                row[currCellNumber] = human.Work[circle].JobTitle;
                row[currCellNumber + 1] = human.Work[circle].Conpany;
                row[currCellNumber + 2] = human.Work[circle].StartDate;
                row[currCellNumber + 3] = human.Work[circle].EndDate;
                circle++;
            }

            circle = 0;
            for (int currCellNumber = firstEducationCellNumber; currCellNumber < maxEduCell; currCellNumber += 4)
            {
                row[currCellNumber] = human.Education[circle].School;
                row[currCellNumber + 1] = human.Education[circle].Degree;
                row[currCellNumber + 2] = human.Education[circle].StartDate;
                row[currCellNumber + 3] = human.Education[circle].EndDate;
                circle++;
            }

            _csvFile.Rows.Add(row.ToList());
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (_runningState == false)
            {
                button1.PerformClick();
            }

            _workerThread.Abort();

            Brwsr.Close();

        }
    }
}
