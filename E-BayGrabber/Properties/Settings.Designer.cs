﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace E_BayGrabber.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "14.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://www.ebay.com/sch/Laptops-Netbooks/175672/i.html?_from=R40&LH_BIN=1&_sop=15" +
            "&_mPrRngCbx=1&_udlo=80&_udhi=&_nkw=macbook")]
        public string Url {
            get {
                return ((string)(this["Url"]));
            }
            set {
                this["Url"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("ff")]
        public string Browser {
            get {
                return ((string)(this["Browser"]));
            }
            set {
                this["Browser"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("c:\\")]
        public string PathToSaveRezults {
            get {
                return ((string)(this["PathToSaveRezults"]));
            }
            set {
                this["PathToSaveRezults"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("EbayGrabbingRezults")]
        public string RezultFileName {
            get {
                return ((string)(this["RezultFileName"]));
            }
            set {
                this["RezultFileName"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("5")]
        public int WaitPeriod {
            get {
                return ((int)(this["WaitPeriod"]));
            }
            set {
                this["WaitPeriod"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("MC721LL/A\r\nMB990LL/A\r\nMC721LL/A\r\nMC965LL/A\r\nMC968LL/A\r\nMC374LL/A\r\nMC118LL/A\r\nMD10" +
            "4LL/A\r\nMD313LL/A\r\nMC700LL/A\r\nMC207LL/A\r\nMD231ll/A\r\nMD103LL/A\r\nMB986LL/A\r\nMD711LL" +
            "/B\r\nMC240LL/A\r\nMD102LL/A\r\nMC976LL/A\r\nMC966LL/A\r\nMD223LL/A\r\nMC503LL/A\r\nMD760LL/A\r" +
            "\nMD711LL/A\r\nMC371LL/A\r\nMC975LL/A\r\nMC516LL/A\r\nMB991LL/A\r\nMC373LL/A\r\nMB467LL/A\r\nMB" +
            "466LL/A\r\nMD318LL/A\r\nMB985LL/A\r\nME665LL/A\r\nMD761LL/B\r\nMC505LL/A\r\nMC724LL/A\r\nMD760" +
            "LL/B,\r\nME864LL/A\r\nMC372LL/A\r\nMB470LL/A\r\nME865LL/A\r\nMD311LL/A\r\nMD761LL/A\r\nMD322LL" +
            "/A\r\nMB402LL/A\r\nMGX72LL/A\r\nMB133LL/A\r\nMC723LL/A\r\nME293LL/A\r\nME664LL/A\r\nMD224LL/A\r" +
            "\nMB471LL/A\r\nMC226LL/A\r\nME294LL/A\r\nMD232LL/A\r\nMD212LL/A\r\n13-inch\r\nMD314LL/A\r\nMC96" +
            "9LL/A\r\nMC725LL/A\r\nMD712LL/B\r\nMC233LL/A\r\nMB061LL/A\r\nMB881LL/A\r\nMC026LL/A\r\nME662LL" +
            "/A\r\nMB604LL/A\r\nMC024LL/A\r\nMA092LL/A\r\nMGX82LL/A\r\nMD712LL/A\r\nMB766LL/A\r\nME866LL/A\r" +
            "\nMB134LL/A\r\nMC506LL/A\r\nMD213LL/A\r\nMB076LL/A\r\nMC504LL/A\r\nMC234LL/A\r\nZ0J84LL/A\r\nZ0" +
            "J62LL/A\r\nMC721LL/A\r\nMB990LL/A\r\nMC721LL/A\r\nMC965LL/A\r\nMC968LL/A\r\nMC374LL/A\r\nMC118" +
            "LL/A\r\nMD104LL/A\r\nMD313LL/A\r\nMC700LL/A\r\nMC207LL/A\r\nMD231ll/A\r\nMD103LL/A\r\nMB986LL/" +
            "A\r\nMD711LL/B\r\nMC240LL/A\r\nMD102LL/A\r\nMC976LL/A\r\nMC966LL/A\r\nMD223LL/A\r\nMC503LL/A\r\n" +
            "MD760LL/A\r\nMD711LL/A\r\nMC371LL/A\r\nMC975LL/A\r\nMC516LL/A\r\nMB991LL/A\r\nMC373LL/A\r\nMB4" +
            "67LL/A\r\nMB466LL/A\r\nMD318LL/A\r\nMB985LL/A\r\nME665LL/A\r\nMD761LL/B\r\nMC505LL/A\r\nMC724L" +
            "L/A\r\nMD760LL/B,\r\nME864LL/A\r\nMC372LL/A\r\nMB470LL/A\r\nME865LL/A\r\nMD311LL/A\r\nMD761LL/" +
            "A\r\nMD322LL/A\r\nMB402LL/A\r\nMGX72LL/A\r\nMB133LL/A\r\nMC723LL/A\r\nME293LL/A\r\nME664LL/A\r\n" +
            "MD224LL/A\r\nMB471LL/A\r\nMC226LL/A\r\nME294LL/A\r\nMD232LL/A\r\nMD212LL/A\r\n13-inch\r\nMD314" +
            "LL/A\r\nMC969LL/A\r\nMC725LL/A\r\nMD712LL/B\r\nMC233LL/A\r\nMB061LL/A\r\nMB881LL/A\r\nMC026LL/" +
            "A\r\nME662LL/A\r\nMB604LL/A\r\nMC024LL/A\r\nMA092LL/A\r\nMGX82LL/A\r\nMD712LL/A\r\nMB766LL/A\r\n" +
            "ME866LL/A\r\nMB134LL/A\r\nMC506LL/A\r\nMD213LL/A\r\nMB076LL/A\r\nMC504LL/A\r\nMC234LL/A\r\nZ0J" +
            "84LL/A\r\nZ0J62LL/A")]
        public string KeyWords {
            get {
                return ((string)(this["KeyWords"]));
            }
            set {
                this["KeyWords"] = value;
            }
        }
    }
}
