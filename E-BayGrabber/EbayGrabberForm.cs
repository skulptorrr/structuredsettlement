﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using BotAgent.Ifrit.Core;
using BotAgent.Ifrit.Core.BaseClasses;
using BotAgent.Ifrit.Core.ElemTypes;
using BotAgent.Ifrit.Exceptions;
using E_BayGrabber.Properties;
using System.Globalization;
using BotAgent.DataExporter;

namespace E_BayGrabber
{
    public partial class EbayGrabberForm : Form
    {
        private static List<double> _prices = new List<double>();
        private static Csv _csvFile = new Csv();
        private Thread _workerThread;

        private bool _isRunning = true;
        
        private static IfrBrowser Brwsr;

        public EbayGrabberForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _workerThread = new Thread(DoWork);
            _workerThread.Start();
        }

        private void DoWork()
        {
            StartWork();

            SaveCsvToHdd();

            string[] keyWords = Regex.Split(Settings.Default.KeyWords, "\r\n");

            Brwsr.Nav.GoToAndWaitForPageLoad(Settings.Default.Url);

            Thread.Sleep(800);

            foreach (string keyword in keyWords)
            {
                DoSearchByKeyword(keyword);

                //var manufacturerChkBox = Brwsr.Page.Elem(By.Xpath(".//span[@class='cbx' and text() = 'Manufacturer refurbished']"), Await.Present(5000));
                
                //var manufacturerRefurbishedExist = manufacturerChkBox.AsButton().IsExist();

                //if (manufacturerRefurbishedExist)
                //{
                //    GoToManufacturerRefutbished(manufacturerChkBox);
                //    //Parse5PricesFromPage();
                //}

                GoToNewAndUsedPrices();
                Parse5PricesFromPage();

                AddRowFromPrices();

                SaveCsvToHdd();

                Thread.Sleep(Settings.Default.WaitPeriod * 1000);
            }

            EndWork();
        }

        private static void GoToNewAndUsedPrices()
        {
            Brwsr.Page.Elem(By.Xpath(".//span[@id='LH_ItemCondition']/../span[@class='seeall']/button"),Await.Visible(5000)).AsButton().ClickAndWaitForAjax();

            Brwsr.Page.Elem(By.Xpath(".//label[@title='New']"), Await.Visible(2000)).AsButton().Click();
            Brwsr.Page.Elem(By.Xpath(".//label[@title='Used']"), Await.Visible(2000)).AsButton().Click();
            Brwsr.Page.Elem(By.Xpath(".//label[@title='Seller refurbished']"), Await.Visible(2000)).AsButton().Click();
            Brwsr.Page.Elem(By.Xpath(".//label[@title='Manufacturer refurbished']"), Await.Visible(2000)).AsButton().Click();
            Thread.Sleep(200);
            Brwsr.Page.Elem(By.Xpath(".//input[@class='submit-btn']")).AsButton().ClickAndWaitForPageLoad();

            SetLowestPrice();
        }

        private static void GoToManufacturerRefutbished(ElemType checkboxElem)
        {
            checkboxElem.AsButton().Click();
            SetLowestPrice();
        }

        private static void Parse5PricesFromPage()
        {
            double price = 0;
            double itemDouPrice = 0;
            double shippingDouPrice = 0;

            var prices = Brwsr.Page.Elems(By.Xpath(".//li[@class='lvprice prc']/span")).AsList();
            var shippingPrice = Brwsr.Page.Elems(By.Xpath(".//li[@class='lvshipping']/span")).AsList();

            var circles = prices.Count;
            if (circles > 5) circles = 5;
            
            for (int i = 0; i < circles; i++)
            {
                price = itemDouPrice = shippingDouPrice = 0;

                var priceStr = prices[i].AsOther().TextOfTagsTree;

                var shippingPriceStr = shippingPrice[i].AsOther().TextOfTagsTree;
                
                if (shippingPriceStr.Contains("+$") && shippingPriceStr.Contains("shipping"))
                {
                    shippingPriceStr = shippingPriceStr.Replace("+$", "").Replace("shipping", "").Trim(' ');//.Replace(".",",");
                }
                else
                {
                    shippingPriceStr = "0,0";
                }

                if (priceStr.Contains("to")) priceStr = priceStr.Replace("to","|").Split('|')[0];

                priceStr = priceStr.Trim('\t').Trim(' ').Replace(",", "").Replace("$", "");//.Replace(".", ",");
                
                priceStr = (priceStr.Contains("\r")) ? priceStr.Substring(0, priceStr.IndexOf('\r')) : priceStr;
                
                priceStr = (priceStr.Contains("<")) ? priceStr.Substring(0, priceStr.IndexOf('<')) : priceStr;

                if (priceStr.Length > 0)
                {
                    itemDouPrice = GetDouble(priceStr, 0);
                    shippingDouPrice = GetDouble(shippingPriceStr, 0);
                    price = itemDouPrice + shippingDouPrice;
                }
                
                _prices.Add(price);
            }
        }

        public static double GetDouble(string value, double defaultValue)
        {
            double result = defaultValue;

            double.TryParse(value, System.Globalization.NumberStyles.Any, CultureInfo.CurrentCulture, out result);

            //Try parsing in the current culture
            //if (!double.TryParse(value, System.Globalization.NumberStyles.Any, CultureInfo.CurrentCulture, out result) &&
            //    //Then try in US english
            //    !double.TryParse(value, System.Globalization.NumberStyles.Any, CultureInfo.GetCultureInfo("en-US"), out result) &&
            //    //Then in neutral language
            //    !double.TryParse(value, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out result))
            //{
            //    result = defaultValue;
            //}

            return result;
        }

        private static void SetLowestPrice()
        {
            // Format -> Buy it now only
            if (Brwsr.Page.Elem(By.Xpath(".//span[text()='Buy It Now']/../../input")).AsOther().Selected == false )
                Brwsr.Page.Elem(By.Xpath(".//span[text()='Buy It Now']")).AsButton().Click();

            Thread.Sleep(800);

            // Lowest price
            Brwsr.Page.Elem(By.Xpath("id('SortMenu')/../ul[1]"), Await.Visible(5000)).AsLink().Click();
            Brwsr.Page.Elem(By.Xpath("id('SortMenu')/li/a[text()='Price + Shipping: lowest first']")).AsLink().ClickAndWaitForPageLoad();

        }

        private static void AddRowFromPrices()
        {
            string productName = Brwsr.Page.Elem(By.Id("gh-ac")).AsInput().GetAttribute("value");

            _prices.Sort();

            var pricesList = _prices.Select(i => i.ToString()).ToList();
            pricesList.Insert(0, productName);

            _csvFile.Rows.Add(pricesList);

            _prices.Clear();
        }

        private static void DoSearchByKeyword(string keyword)
        {
            Brwsr.Page.Elem(By.Id("gh-ac")).AsInput().SetTextAndSubmit(keyword);

            Thread.Sleep(500);
        }

        private void SaveCsvToHdd()
        {
            try
            {
                _csvFile.FileSave(Settings.Default.PathToSaveRezults.TrimEnd('\\') + "\\" + Settings.Default.RezultFileName + ".csv");
            }
            catch (Exception)
            {
                Brwsr.Close();
                throw new Exception("Failed to save CSV file. Looks like you forget to run program as Administrator or set incorrect filepath.");
            }
            
        }

        private void StartWork()
        {
            Brwsr = new IfrBrowser();

            button1.Enabled = true;
        }

        private void EndWork()
        {
            SaveCsvToHdd();

            Brwsr.Close();

            Invoke(new Action(() => Close()));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (_isRunning)
            {
                _workerThread.Suspend();
                SaveCsvToHdd();
                button1.Text = "Resume";
                _isRunning = false;
            }
            else
            {
                _workerThread.Resume();
                button1.Text = "Pause";
                _isRunning = true;
            }
        }

    }
}
