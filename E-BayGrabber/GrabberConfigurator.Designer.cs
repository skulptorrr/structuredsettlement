﻿namespace E_BayGrabber
{
    partial class GrabberConfigurator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.Url = new System.Windows.Forms.TextBox();
            this.ff = new System.Windows.Forms.RadioButton();
            this.chrome = new System.Windows.Forms.RadioButton();
            this.fjs = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.RezPath = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.FileName = new System.Windows.Forms.TextBox();
            this.Save = new System.Windows.Forms.Button();
            this.Delay = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.KeyWodsFld = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Delay)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "URL";
            // 
            // Url
            // 
            this.Url.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Url.Location = new System.Drawing.Point(49, 35);
            this.Url.Name = "Url";
            this.Url.Size = new System.Drawing.Size(503, 20);
            this.Url.TabIndex = 1;
            // 
            // ff
            // 
            this.ff.AutoSize = true;
            this.ff.Checked = true;
            this.ff.Location = new System.Drawing.Point(16, 12);
            this.ff.Name = "ff";
            this.ff.Size = new System.Drawing.Size(53, 17);
            this.ff.TabIndex = 2;
            this.ff.TabStop = true;
            this.ff.Text = "firefox";
            this.ff.UseVisualStyleBackColor = true;
            // 
            // chrome
            // 
            this.chrome.AutoSize = true;
            this.chrome.Enabled = false;
            this.chrome.Location = new System.Drawing.Point(75, 12);
            this.chrome.Name = "chrome";
            this.chrome.Size = new System.Drawing.Size(60, 17);
            this.chrome.TabIndex = 3;
            this.chrome.Text = "chrome";
            this.chrome.UseVisualStyleBackColor = true;
            // 
            // fjs
            // 
            this.fjs.AutoSize = true;
            this.fjs.Location = new System.Drawing.Point(141, 12);
            this.fjs.Name = "fjs";
            this.fjs.Size = new System.Drawing.Size(107, 17);
            this.fjs.TabIndex = 4;
            this.fjs.Text = "headless browser";
            this.fjs.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 359);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Results Folder";
            // 
            // RezPath
            // 
            this.RezPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RezPath.Location = new System.Drawing.Point(95, 356);
            this.RezPath.Name = "RezPath";
            this.RezPath.Size = new System.Drawing.Size(456, 20);
            this.RezPath.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 385);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "FileName";
            // 
            // FileName
            // 
            this.FileName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.FileName.Location = new System.Drawing.Point(75, 385);
            this.FileName.Name = "FileName";
            this.FileName.Size = new System.Drawing.Size(185, 20);
            this.FileName.TabIndex = 8;
            // 
            // Save
            // 
            this.Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Save.Location = new System.Drawing.Point(441, 383);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(110, 23);
            this.Save.TabIndex = 9;
            this.Save.Text = "Save";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // Delay
            // 
            this.Delay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Delay.Location = new System.Drawing.Point(363, 386);
            this.Delay.Name = "Delay";
            this.Delay.Size = new System.Drawing.Size(56, 20);
            this.Delay.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(269, 388);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Delay in seconds";
            // 
            // KeyWodsFld
            // 
            this.KeyWodsFld.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.KeyWodsFld.Location = new System.Drawing.Point(16, 77);
            this.KeyWodsFld.Multiline = true;
            this.KeyWodsFld.Name = "KeyWodsFld";
            this.KeyWodsFld.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.KeyWodsFld.Size = new System.Drawing.Size(536, 273);
            this.KeyWodsFld.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(137, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "KeyWords: (One per string):";
            // 
            // GrabberConfigurator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(563, 414);
            this.Controls.Add(this.KeyWodsFld);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Delay);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.FileName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.RezPath);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.fjs);
            this.Controls.Add(this.chrome);
            this.Controls.Add(this.ff);
            this.Controls.Add(this.Url);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(579, 205);
            this.Name = "GrabberConfigurator";
            this.Text = "GrabberConfigurator";
            this.Load += new System.EventHandler(this.GrabberConfigurator_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Delay)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Url;
        private System.Windows.Forms.RadioButton ff;
        private System.Windows.Forms.RadioButton chrome;
        private System.Windows.Forms.RadioButton fjs;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox RezPath;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox FileName;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.NumericUpDown Delay;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox KeyWodsFld;
        private System.Windows.Forms.Label label5;
    }
}