﻿using System;
using System.Security.Policy;
using System.Windows.Forms;
using E_BayGrabber.Properties;

namespace E_BayGrabber
{
    public partial class GrabberConfigurator : Form
    {
        public GrabberConfigurator()
        {
            InitializeComponent();
        }

        private void GrabberConfigurator_Load(object sender, EventArgs e)
        {
            switch (Settings.Default.Browser)
            {
                case ("ff"):
                    ff.Checked = true;
                    break;
                case ("chrome"):
                    chrome.Checked = true;
                    break;
                case ("fjs"):
                    fjs.Checked = true;
                    break;
            }

            RezPath.Text = Settings.Default.PathToSaveRezults;

            Url.Text = Settings.Default.Url;

            FileName.Text = Settings.Default.RezultFileName;

            Delay.Value = Settings.Default.WaitPeriod;

            KeyWodsFld.Text = Settings.Default.KeyWords;

        }

        private void Save_Click(object sender, EventArgs e)
        {
            if (ff.Checked)
            {
                Settings.Default.Browser = "ff";
            }
            else if (chrome.Checked == true)
            {
                Settings.Default.Browser = "chrome";
            }
            else if (fjs.Checked == true )
            {
                Settings.Default.Browser = "fjs";
            }

            Settings.Default.PathToSaveRezults = RezPath.Text;

            Settings.Default.Url = Url.Text;

            Settings.Default.RezultFileName = FileName.Text;

            Settings.Default.KeyWords = KeyWodsFld.Text;

            Settings.Default.WaitPeriod = (int) Delay.Value;

            Settings.Default.Save();

            this.Close();
        }
    }
}
