﻿using System;
using System.Windows.Forms;
using NewYorkGrabber.Properties;

namespace NewYorkGrabber
{
    public partial class Configuration : Form
    {
        public Configuration()
        {
            InitializeComponent();
        }

        private void Save_Click(object sender, EventArgs e)
        {
            Settings.Default.csvPath = csvPathFld.Text;

            Settings.Default.url = LinkFld.Text;

            Settings.Default.Save();

            Close();
        }

        private void Configuration_Load(object sender, EventArgs e)
        {
            csvPathFld.Text = Settings.Default.csvPath;

            LinkFld.Text = Settings.Default.url;
        }
    }
}
