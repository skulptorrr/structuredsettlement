﻿using BotAgent.Ifrit.Core;
using BotAgent.Ifrit.Core.BaseClasses;
using BotAgent.DataExporter;
using System;
using System.Threading;
using System.Windows.Forms;
using NewYorkGrabber.Properties;
using System.Collections.Generic;

namespace NewYorkGrabber
{
    public partial class NewYorkGrabber : Form
    {
        Thread workThread;
        Csv csv = new Csv();

        public NewYorkGrabber()
        {
            InitializeComponent();
        }

        private void StartBtn_Click(object sender, EventArgs e)
        {
            switch (StartBtn.Text)
            {
                case "Grab!":
                    StartBtn.Text = "Pause";

                    workThread = new Thread(StartWork);
                    workThread.Start();
                    break;

                case "Resume":
                    StartBtn.Text = "Pause";

                    ResumeWork();
                    break;

                case "Pause":
                    StartBtn.Text = "Resume";

                    PauseWork();
                    break;
            }
        }

        private void StartWork()
        {
            DoWork();
            
            Invoke((MethodInvoker)delegate { 
                StartBtn.Text = "Grab!";
            });
        }

        private void DoWork()
        {
            IfrBrowser brwsr = new IfrBrowser();
            
            brwsr.Nav.GoTo(Settings.Default.url);

            brwsr.Page.Elem(By.Id("listview")).AsButton().ClickAndWaitForAjax();

            do
            {
                ParseSinglePage(brwsr);

                brwsr.Page.Elem(By.Xpath(".//a[@class='button next']")).AsButton().ClickAndWaitForAjax();
                
            }
            while (brwsr.Page.Elem(By.Xpath(".//a[@class='button next']"), Await.Present()).AsButton().IsVisible());

            csv.FileSave(Settings.Default.csvPath);
            brwsr.Close();
        }

        private void ParseSinglePage(IfrBrowser brwsr)
        {
            string time;
            string title;
            string price;
            string pic;

            int infoBlocksNumber = brwsr.Page.Elems(By.Class("result-row")).Count;//there is one hidden row allways

            for (int i = 1; i <= infoBlocksNumber; i++)
            {
                time = brwsr.Page.Elem(By.Xpath(".//li[" + i + "]/p/time")).AsOther().GetAttribute("datetime");
                title = brwsr.Page.Elem(By.Xpath(".//li[" + i + "]/p/a")).AsOther().Text;
                price = brwsr.Page.Elem(By.Xpath(".//li[" + i + "]/p//span[@class='result-price']")).AsOther().Text;
                pic = brwsr.Page.Elem(By.Xpath(".//li[" + i + "]//img")).AsImage().SrcValue;

                csv.Rows.Add(new List<string>() {time, title, price, pic});
            }
        }

        private void ResumeWork()
        {
            workThread.Resume();
        }

        private void PauseWork()
        {
            workThread.Suspend();

            csv.FileSave(Settings.Default.csvPath);
            
        }

        private void NewYorkGrabber_Load(object sender, EventArgs e)
        {
            StartBtn.PerformClick();
        }

    }
}
