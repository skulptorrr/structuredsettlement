﻿using System;
using System.Windows.Forms;
using FileSystem;

namespace NewYorkGrabber
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (args.Length != 0)
            {
                if (args[0] == "/config")
                {
                    Application.Run(new Configuration());
                }
            }
            else
            {
                if (FS.IsAdministrator())
                {
                    FS.CleanUpSeleniumTmpFiles();

                    Application.Run(new NewYorkGrabber());
                }   
            }
        }

    }
}
