﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatchGrabber
{
    public class GrabRezult
    {
       
        public string PageUrl;
        public string Nick;
        public string ShortDescription;
        
        public string ApperanceHeightLookFor;
        public string ApperanceHeightShe;
        public string ApperanceBodyTypeLookFor;
        public string ApperanceBodyTypeShe;
        public string ApperanceEyesLookFor;
        public string ApperanceEyesShe;
        public string ApperanceHairLookFor;
        public string ApperanceHairShe;
        
        public string LifeStyleSmokeLookFor;
        public string LifeStyleSmokeShe;
        public string LifeStyleDrinkLookFor;
        public string LifeStyleDrinkShe;
        public string LifeStyleOccupationLookFor;
        public string LifeStyleOccupationShe;
        public string LifeStyleIncomeLookFor;
        public string LifeStyleIncomeShe;
        public string LifeStyleRelationShipLookFor;
        public string LifeStyleRelationShipShe;
        public string LifeStyleKidsLookFor;
        public string LifeStyleKidsShe;
        public string LifeStyleWantKidsLookFor;
        public string LifeStyleWantKidsShe;
        
        public string BackGroundEthnicityLookFor;
        public string BackGroundEthnicityShe;
        public string BackGroundReligionLookFor;
        public string BackGroundReligionShe;
        public string BackGroundLanguagesLookFor;
        public string BackGroundLanguagesShe;
        public string BackGroundEducationLookFor;
        public string BackGroundEducationShe;
    }
}
