﻿using BotAgent.Ifrit.Core;
using BotAgent.Ifrit.Core.BaseClasses;
using MatchGrabber.Properties;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace MatchGrabber
{
    public partial class Form1
    {
        private IfrPage Page
        {
            get { return Brwsr.Page; }            
        }
        
        private void GoToSiteAndLogin()
        {
            Brwsr.Nav.GoToAndWaitForPageLoad("http://www.match.com/login/login.aspx");

            Page.Elem(By.Id("email")).AsInput().Text = Settings.Default.Login;
            var passFld = Page.Elem(By.Id("password")).AsInput();

            passFld.Text = Settings.Default.Password;
            passFld.SendEnterAndWaitForPageLoad();
        }

        private void Wait4UserAction()
        {
            MessageBox.Show("Click OK when you will be On SearchRez page");
        }

        private List<string> _grabbedItemsStyles = new List<string>();
        private List<string> _profilesLinks = new List<string>();

        private void GrabResults4CountOfScrens(int screensCount)
        {
            for (int i = 1; i <= screensCount; i++)
            {
                GrabNextNextSearchRezProfiles();
                SaveResults();
            }
        }

        private void GrabNextNextSearchRezProfiles()
        {
            var styles = Page.Elems(By.Xpath("//div[@class='ReactVirtualized__Grid__innerScrollContainer']/div")).AsListOfAttr("style");

            var xPath1 = "//div[@class='ReactVirtualized__Grid__innerScrollContainer']/div[@style='";
            var xPath3 = "']/div/div/div[3]/a";

            foreach (var style in styles)
            {
                if (!_grabbedItemsStyles.Contains(style))
                {
                    var fullXpath = xPath1 + style + xPath3;

                    var hrefs = Page.Elems(By.Xpath(fullXpath)).AsListOfHrefs();

                    Uri myUri = new Uri(Page.Url);
                    var domain = myUri.Host;

                    foreach (var href in hrefs)
                    {
                        _profilesLinks.Add(href);
                    }

                    _grabbedItemsStyles.Add(style);
                }
            }

            Page.Elem(By.Xpath("//body")).AsOther().SendPgDown();
        }

        private void GrabProfiles()
        {
            foreach (var profileUrl in _profilesLinks)
            {
                var tmpRez = GrabSinglePage(profileUrl);

                _resultList.Add(tmpRez);

                Thread.Sleep(Settings.Default.Delay * 1000);
            }
        }

        private GrabRezult GrabSinglePage(string url)
        {
            GrabRezult rez = new GrabRezult();

            Brwsr.Nav.GoToAndWaitForPageLoad(url);

            rez.PageUrl = url;

            rez.ShortDescription = Page.Elem(By.Xpath("//main/article/section[1]/div[1]/div[1]/div[2]/section/ul")).AsOther().TextOfTagsTree.Replace("\r\n","; ");

            rez.Nick = Page.Elem(By.Xpath("//main/article/section[1]/div[1]/div[1]/div[2]//text")).AsOther().TextOfTagsTree;

            Page.Elem(By.Xpath("//div[text()='See More']/..")).AsButton().Click();

            //Apperance
            rez.ApperanceHeightLookFor = Page.Elem(By.Xpath("//div[text()='Appearance']/../div[3]/div[1]")).AsOther().TextOfTagsTree.Replace("\r\n"," ");
            rez.ApperanceHeightShe = Page.Elem(By.Xpath("//div[text()='Appearance']/../div[3]/div[2]")).AsOther().TextOfTagsTree;

            rez.ApperanceBodyTypeLookFor = Page.Elem(By.Xpath("//div[text()='Appearance']/../div[4]/div[1]")).AsOther().TextOfTagsTree;
            rez.ApperanceBodyTypeShe = Page.Elem(By.Xpath("//div[text()='Appearance']/../div[4]/div[2]")).AsOther().TextOfTagsTree;

            rez.ApperanceEyesLookFor = Page.Elem(By.Xpath("//div[text()='Appearance']/../div[5]/div[1]")).AsOther().TextOfTagsTree;
            rez.ApperanceEyesShe = Page.Elem(By.Xpath("//div[text()='Appearance']/../div[5]/div[2]")).AsOther().TextOfTagsTree;

            rez.ApperanceHairLookFor = Page.Elem(By.Xpath("//div[text()='Appearance']/../div[6]/div[1]")).AsOther().TextOfTagsTree;
            rez.ApperanceHairShe = Page.Elem(By.Xpath("//div[text()='Appearance']/../div[6]/div[2]")).AsOther().TextOfTagsTree;

            //LifeStyle
            rez.LifeStyleSmokeLookFor = Page.Elem(By.Xpath("//div[text()='Lifestyle']/../div[3]/div[1]")).AsOther().TextOfTagsTree;
            rez.LifeStyleSmokeShe = Page.Elem(By.Xpath("//div[text()='Lifestyle']/../div[3]/div[2]")).AsOther().TextOfTagsTree;
            
            rez.LifeStyleDrinkLookFor = Page.Elem(By.Xpath("//div[text()='Lifestyle']/../div[4]/div[1]")).AsOther().TextOfTagsTree;
            rez.LifeStyleDrinkShe = Page.Elem(By.Xpath("//div[text()='Lifestyle']/../div[4]/div[2]")).AsOther().TextOfTagsTree;

            rez.LifeStyleOccupationLookFor = Page.Elem(By.Xpath("//div[text()='Lifestyle']/../div[5]/div[1]")).AsOther().TextOfTagsTree;
            rez.LifeStyleOccupationShe = Page.Elem(By.Xpath("//div[text()='Lifestyle']/../div[5]/div[2]")).AsOther().TextOfTagsTree;

            rez.LifeStyleIncomeLookFor = Page.Elem(By.Xpath("//div[text()='Lifestyle']/../div[6]/div[1]")).AsOther().TextOfTagsTree;
            rez.LifeStyleIncomeShe = Page.Elem(By.Xpath("//div[text()='Lifestyle']/../div[6]/div[2]")).AsOther().TextOfTagsTree;

            rez.LifeStyleRelationShipLookFor = Page.Elem(By.Xpath("//div[text()='Lifestyle']/../div[7]/div[1]")).AsOther().TextOfTagsTree;
            rez.LifeStyleRelationShipShe = Page.Elem(By.Xpath("//div[text()='Lifestyle']/../div[7]/div[2]")).AsOther().TextOfTagsTree;

            rez.LifeStyleKidsLookFor = Page.Elem(By.Xpath("//div[text()='Lifestyle']/../div[8]/div[1]")).AsOther().TextOfTagsTree;
            rez.LifeStyleKidsShe = Page.Elem(By.Xpath("//div[text()='Lifestyle']/../div[8]/div[2]")).AsOther().TextOfTagsTree;

            rez.LifeStyleWantKidsLookFor = Page.Elem(By.Xpath("//div[text()='Lifestyle']/../div[9]/div[1]")).AsOther().TextOfTagsTree;
            rez.LifeStyleWantKidsShe = Page.Elem(By.Xpath("//div[text()='Lifestyle']/../div[9]/div[2]")).AsOther().TextOfTagsTree;

            //Background
            rez.BackGroundEthnicityLookFor = Page.Elem(By.Xpath("//div[text()='Background']/../div[3]/div[1]")).AsOther().TextOfTagsTree;
            rez.BackGroundEthnicityShe = Page.Elem(By.Xpath("//div[text()='Background']/../div[3]/div[2]")).AsOther().TextOfTagsTree;

            rez.BackGroundReligionLookFor = Page.Elem(By.Xpath("//div[text()='Background']/../div[4]/div[1]")).AsOther().TextOfTagsTree;
            rez.BackGroundReligionShe = Page.Elem(By.Xpath("//div[text()='Background']/../div[4]/div[2]")).AsOther().TextOfTagsTree;

            rez.BackGroundLanguagesLookFor = Page.Elem(By.Xpath("//div[text()='Background']/../div[5]/div[1]")).AsOther().TextOfTagsTree;
            rez.BackGroundLanguagesShe = Page.Elem(By.Xpath("//div[text()='Background']/../div[5]/div[2]")).AsOther().TextOfTagsTree;

            rez.BackGroundEducationLookFor = Page.Elem(By.Xpath("//div[text()='Background']/../div[6]/div[1]")).AsOther().TextOfTagsTree;
            rez.BackGroundEducationShe = Page.Elem(By.Xpath("//div[text()='Background']/../div[6]/div[2]")).AsOther().TextOfTagsTree;


            return rez;
        }
    }
}
