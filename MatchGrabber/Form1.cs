﻿using BotAgent.DataExporter;
using BotAgent.Ifrit.Core;
using MatchGrabber.Properties;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace MatchGrabber
{
    public partial class Form1 : Form
    {
        private Thread _workerThread;
        private IfrBrowser Brwsr;

        private List<GrabRezult> _resultList = new List<GrabRezult>();

        private Csv _csv = new Csv();

        private bool _runningState = true;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (_runningState)
            {
                _workerThread.Suspend();

                SaveResults();

                button1.Text = "Resume";
                _runningState = false;
            }
            else
            {
                _workerThread.Resume();
                button1.Text = "Pause & save results";
                _runningState = true;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _workerThread = new Thread(DoWork);
            _workerThread.Start();
        }

        private void DoWork()
        {
            StartWork();
            MainBlockWork();
            EndWork();
        }

        private void StartWork()
        {
            Brwsr = new IfrBrowser();
        }

        private void MainBlockWork()
        {
            GoToSiteAndLogin();

            Brwsr.Nav.GoToAndWaitForPageLoad("https://www4.match.com/search/saved/list");

            Wait4UserAction();

            GrabResults4CountOfScrens(Settings.Default.ScreensToGrab);

            GrabProfiles();
        }

        private void EndWork()
        {
            Brwsr.Close();
            SaveResults();
            Application.Exit();
        }

        private void SaveResults()
        {
            var filePath = Settings.Default.FilePath.TrimEnd('\\') + "\\" + Settings.Default.FileName;

            try
            {
                _csv.FileOpen(filePath);
            }
            catch (Exception e)
            { }

            if (_csv.Rows.Count <= 1)
            {
                AddFirstExcelRow();
            }

            for (int i = 0; i < _resultList.Count; i++)
            {
                var row = GetExcelRowFromGrabRez(_resultList[i]);

                _csv.AddRow(row.ToArray());
            }

            _resultList = new List<GrabRezult>();

            //try
            //{
                _csv.FileSave(filePath);
            //}
            //catch (Exception e)
            //{
            //    MessageBox.Show("No ability to save file into selected path. Possibly you have run app not as Admin. Run as Admin or choose path accessible for current user;");
            //}

            

        }

        private void AddFirstExcelRow()
        {
            var ap = "Apperance|";
            var ls = "LifeStyle|";
            var bg = "BackGround|";

            var lf = "LookFor|";
            var sh = "She|";

            _csv.AddRow("Nick","Url","Short Description",
                ap+lf + "Height",
                ap+sh + "Height",

                ap + lf + "BodyType",
                ap + sh + "BodyType",
                ap + lf + "Eyes",
                ap + sh + "Eyes",
                ap + lf + "Hair",
                ap + sh + "Hair",

                ls + lf + "Smoke",
                ls + sh + "Smoke",
                ls + lf + "Drink",
                ls + sh + "Drink",
                ls + lf + "Occupation",
                ls + sh + "Occupation",
                ls + lf + "Income",
                ls + sh + "Income",
                ls + lf + "RelationShip",
                ls + sh + "RelationShip",
                ls + lf + "Kids",
                ls + sh + "Kids",
                ls + lf + "Want Kids",
                ls + sh + "Want Kids",

                bg + lf + "Ethnicity",
                bg + sh + "Ethnicity",
                bg + lf + "Religion",
                bg + sh + "Religion",
                bg + lf + "Languages",
                bg + sh + "Languages",
                bg + lf + "Education",
                bg + sh + "Education"
                );
        }

        private List<string> GetExcelRowFromGrabRez(GrabRezult gr)
        {
            List<string> rez = new List<string>();

            rez.Add(gr.Nick);
            rez.Add(gr.PageUrl);
            rez.Add(gr.ShortDescription);

            rez.Add(gr.ApperanceHeightLookFor);
            rez.Add(gr.ApperanceHeightShe);
            rez.Add(gr.ApperanceBodyTypeLookFor);
            rez.Add(gr.ApperanceBodyTypeShe);
            rez.Add(gr.ApperanceEyesLookFor);
            rez.Add(gr.ApperanceEyesShe);
            rez.Add(gr.ApperanceHairLookFor);
            rez.Add(gr.ApperanceHairShe);

            rez.Add(gr.LifeStyleSmokeLookFor);
            rez.Add(gr.LifeStyleSmokeShe);
            rez.Add(gr.LifeStyleDrinkLookFor);
            rez.Add(gr.LifeStyleDrinkShe);
            rez.Add(gr.LifeStyleOccupationLookFor);
            rez.Add(gr.LifeStyleOccupationShe);
            rez.Add(gr.LifeStyleIncomeLookFor);
            rez.Add(gr.LifeStyleIncomeShe);
            rez.Add(gr.LifeStyleRelationShipLookFor);
            rez.Add(gr.LifeStyleRelationShipShe);
            rez.Add(gr.LifeStyleKidsLookFor);
            rez.Add(gr.LifeStyleKidsShe);
            rez.Add(gr.LifeStyleWantKidsLookFor);
            rez.Add(gr.LifeStyleWantKidsShe);

            rez.Add(gr.BackGroundEthnicityLookFor);
            rez.Add(gr.BackGroundEthnicityShe);
            rez.Add(gr.BackGroundReligionLookFor);
            rez.Add(gr.BackGroundReligionShe);
            rez.Add(gr.BackGroundLanguagesLookFor);
            rez.Add(gr.BackGroundLanguagesShe);
            rez.Add(gr.BackGroundEducationLookFor);
            rez.Add(gr.BackGroundEducationShe);

            return rez;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (_runningState == false)
            {
                button1.PerformClick();
            }

            _workerThread.Abort();

            EndWork();
        }
    }
}
