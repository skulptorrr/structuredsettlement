﻿using MatchGrabber.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MatchGrabber
{
    public partial class Configurator : Form
    {
        public Configurator()
        {
            InitializeComponent();
        }

        private void Configurator_Load(object sender, EventArgs e)
        {
            LoadSettings();
            
        }
        
        private void Save_Click(object sender, EventArgs e)
        {
            SaveSettings();
            Close();
        }

        private void LoadSettings()
        {
            LoginFld.Text = Settings.Default.Login;
            PasswordFld.Text = Settings.Default.Password;

            RezPath.Text = Settings.Default.FilePath;
            RezPath.Text = Settings.Default.FilePath;
            FileNameFld.Text = Settings.Default.FileName;

            nUpDownScreensToGrab.Value = Settings.Default.ScreensToGrab;

            Delay.Value = Settings.Default.Delay;
        }

        private void SaveSettings()
        {
            Settings.Default.Login = LoginFld.Text;
            Settings.Default.Password = PasswordFld.Text;

            Settings.Default.FilePath = RezPath.Text;
            Settings.Default.FilePath = RezPath.Text;
            Settings.Default.FileName = FileNameFld.Text;

            Settings.Default.ScreensToGrab = (int) nUpDownScreensToGrab.Value;
            Settings.Default.Delay = (int) Delay.Value;

            Settings.Default.Save();
        }
    }
}
