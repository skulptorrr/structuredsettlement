﻿namespace MatchGrabber
{
    partial class Configurator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label7 = new System.Windows.Forms.Label();
            this.PasswordFld = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.LoginFld = new System.Windows.Forms.TextBox();
            this.RezPath = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Delay = new System.Windows.Forms.NumericUpDown();
            this.Save = new System.Windows.Forms.Button();
            this.FileNameFld = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.nUpDownScreensToGrab = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.Delay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUpDownScreensToGrab)).BeginInit();
            this.SuspendLayout();
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 35);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 37;
            this.label7.Text = "Password";
            // 
            // PasswordFld
            // 
            this.PasswordFld.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PasswordFld.Location = new System.Drawing.Point(71, 32);
            this.PasswordFld.Name = "PasswordFld";
            this.PasswordFld.Size = new System.Drawing.Size(492, 20);
            this.PasswordFld.TabIndex = 36;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 35;
            this.label6.Text = "Login";
            // 
            // LoginFld
            // 
            this.LoginFld.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LoginFld.Location = new System.Drawing.Point(71, 6);
            this.LoginFld.Name = "LoginFld";
            this.LoginFld.Size = new System.Drawing.Size(492, 20);
            this.LoginFld.TabIndex = 34;
            // 
            // RezPath
            // 
            this.RezPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RezPath.Location = new System.Drawing.Point(92, 58);
            this.RezPath.Name = "RezPath";
            this.RezPath.Size = new System.Drawing.Size(471, 20);
            this.RezPath.TabIndex = 33;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "Results Folder";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(281, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 13);
            this.label4.TabIndex = 42;
            this.label4.Text = "Delay in seconds";
            // 
            // Delay
            // 
            this.Delay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Delay.Location = new System.Drawing.Point(375, 112);
            this.Delay.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.Delay.Name = "Delay";
            this.Delay.Size = new System.Drawing.Size(56, 20);
            this.Delay.TabIndex = 41;
            // 
            // Save
            // 
            this.Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Save.Location = new System.Drawing.Point(453, 109);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(110, 23);
            this.Save.TabIndex = 40;
            this.Save.Text = "Save";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // FileNameFld
            // 
            this.FileNameFld.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.FileNameFld.Location = new System.Drawing.Point(71, 111);
            this.FileNameFld.Name = "FileNameFld";
            this.FileNameFld.Size = new System.Drawing.Size(185, 20);
            this.FileNameFld.TabIndex = 39;
            this.FileNameFld.Text = "matchComResult.xlsx";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 38;
            this.label3.Text = "FileName";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 13);
            this.label1.TabIndex = 44;
            this.label1.Text = "Search Screens To Grab";
            // 
            // nUpDownScreensToGrab
            // 
            this.nUpDownScreensToGrab.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.nUpDownScreensToGrab.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nUpDownScreensToGrab.Location = new System.Drawing.Point(142, 85);
            this.nUpDownScreensToGrab.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nUpDownScreensToGrab.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nUpDownScreensToGrab.Name = "nUpDownScreensToGrab";
            this.nUpDownScreensToGrab.Size = new System.Drawing.Size(56, 20);
            this.nUpDownScreensToGrab.TabIndex = 43;
            this.nUpDownScreensToGrab.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // Configurator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(575, 169);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nUpDownScreensToGrab);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Delay);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.FileNameFld);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.PasswordFld);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.LoginFld);
            this.Controls.Add(this.RezPath);
            this.Controls.Add(this.label2);
            this.Name = "Configurator";
            this.Text = "Configurator";
            this.Load += new System.EventHandler(this.Configurator_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Delay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUpDownScreensToGrab)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox PasswordFld;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox LoginFld;
        private System.Windows.Forms.TextBox RezPath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown Delay;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.TextBox FileNameFld;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nUpDownScreensToGrab;
    }
}