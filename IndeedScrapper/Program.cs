﻿using System;
using System.Windows.Forms;
using FileSystem;

namespace IndeedScrapper
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            if (FS.IsAdministrator())
            {
                FS.CleanUpSeleniumTmpFiles();

                Application.Run(new Form1());
            }
                
        }
    }
}
