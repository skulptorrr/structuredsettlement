﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using BotAgent.Ifrit.Core;
using BotAgent.Ifrit.Core.BaseClasses;
using SelectPdf;
using System.Threading;

namespace IndeedScrapper
{
    public partial class Form1 : Form
    {
        string _appLog;

        string _resultPath;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnCollect_Click(object sender, EventArgs e)
        {
            CollectInfoFromFirstSearchPage();
        }

        private void CollectInfoFromFirstSearchPage()
        {
            List<string> resultList = new List<string>();

            IfrBrowser brwsr = new IfrBrowser();
            _resultPath = $"GrabbingResults - {brwsr.BrwsrSessionId}";

            brwsr.Nav.GoTo(txtBoxStartUrl.Text);
            
            brwsr.Page.WaitForPageLoad();
            
            bool pageIsLast = false;

            int currPage = 1;

            while (pageIsLast == false || currPage> nUdPagesToCollect.Value)
            {
                _appLog += $"\r\n \t\tPage: {currPage}\r\n";

                pageIsLast = CollectInfoFromSearchPage(brwsr);
                currPage += 1;
            }

            brwsr.Close();

            MessageBox.Show("Collecting is done!");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>if page is last</returns>
        public bool CollectInfoFromSearchPage(IfrBrowser brwsr)
        {
            var hrefsElems = brwsr.Page.Elems(By.Xpath(".//h2/a[@class='turnstileLink']"));
            
            var pageUrls = hrefsElems.AsListOfHrefs();
            var pageUrlsTexts = hrefsElems.AsListOfTexts();

            var nextPageLink = brwsr.Page.Elem(By.Xpath(".//span[contains(text(),'Next')]/../..")).AsLink();
            
            SaveAllWebsFromLinks(pageUrls, pageUrlsTexts);
            
            if (nextPageLink.IsExist())
            {
                nextPageLink.ClickAndWaitForPageLoad();
                Thread.Sleep(2000);

                ClosePopupIfNeeded(brwsr);

                return false;
            }

            return true;
        }

        private void SaveAllWebsFromLinks(List<string> pageUrls, List<string> pageUrlsTexts)
        {
            for (int i = 0; i < pageUrlsTexts.Count; i++)
            {
                SaveUrlToFile(pageUrls[i], pageUrlsTexts[i]);
            }
        }

        private void SaveUrlToFile(string url, string fileName)
        {
            fileName = fileName.Replace("/", " - ");

            fileName = fileName.Replace("\\", "")
                .Replace(":", "")
                .Replace(">", "")
                .Replace("<", "")
                .Replace("?", "")
                .Replace("\"", "")
                .Replace("|", "");
            
            try
            {
                HtmlToPdf converter = new HtmlToPdf();

                PdfDocument doc = converter.ConvertUrl(url);

                var filePath = _resultPath + $"/{fileName}.pdf";

                if (System.IO.File.Exists(filePath))
                {
                    _appLog += $"{DateTime.Now.ToString()} [File already exists. No need to download and save one more time] - {fileName}\r\n";
                }
                else
                {
                    doc.Save(filePath);
                    _appLog+= $"{DateTime.Now.ToString()} [success] - {fileName}\r\n";
                }

                

                doc.Close();
            }
            catch(Exception e)
            {
                _appLog += $"{DateTime.Now.ToString()} [Download error!] - {fileName} - {url}\r\n";
            }

            UpdateLogFile();
        }

        private void ClosePopupIfNeeded(IfrBrowser brwsr)
        {
            brwsr.Page.Elem(By.Id("popover-x-button")).AsButton().Click();
        }


        private void UpdateLogFile()
        {
            try
            {
                if (_appLog.Length > 0)
                {
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(_resultPath + "/!!!Log2.txt"))
                    {
                        file.WriteLine(_appLog);
                    }
                }
            }
            catch {
                //Log file has been open with write permissions
            }

            
        }

    }
}
