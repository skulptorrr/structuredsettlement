﻿namespace IndeedScrapper
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCollect = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBoxStartUrl = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.nUdPagesToCollect = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.nUdPagesToCollect)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCollect
            // 
            this.btnCollect.Location = new System.Drawing.Point(83, 68);
            this.btnCollect.Name = "btnCollect";
            this.btnCollect.Size = new System.Drawing.Size(411, 23);
            this.btnCollect.TabIndex = 4;
            this.btnCollect.Text = "Collect";
            this.btnCollect.UseVisualStyleBackColor = true;
            this.btnCollect.Click += new System.EventHandler(this.btnCollect_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Url";
            // 
            // txtBoxStartUrl
            // 
            this.txtBoxStartUrl.Location = new System.Drawing.Point(83, 10);
            this.txtBoxStartUrl.Name = "txtBoxStartUrl";
            this.txtBoxStartUrl.Size = new System.Drawing.Size(411, 20);
            this.txtBoxStartUrl.TabIndex = 6;
            this.txtBoxStartUrl.Text = "http://www.indeed.com/q-trading-l-ny-jobs.html";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "CollectSearchPages";
            // 
            // nUdPagesToCollect
            // 
            this.nUdPagesToCollect.Location = new System.Drawing.Point(124, 35);
            this.nUdPagesToCollect.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nUdPagesToCollect.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nUdPagesToCollect.Name = "nUdPagesToCollect";
            this.nUdPagesToCollect.Size = new System.Drawing.Size(45, 20);
            this.nUdPagesToCollect.TabIndex = 8;
            this.nUdPagesToCollect.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(506, 139);
            this.Controls.Add(this.nUdPagesToCollect);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtBoxStartUrl);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnCollect);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.nUdPagesToCollect)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnCollect;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBoxStartUrl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nUdPagesToCollect;
    }
}

