﻿using BotAgent.DataExporter;
using System;
using System.IO;
using System.Net;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Windows.Forms;
using Microsoft.VisualBasic.FileIO;

namespace FileSystem
{
    public static class FS
    {
        //public static bool CheckPathForWritePermissions(string path)
        //{ 
        //    var pathIsWritePermitted = HasWritePermissionOnDir(path);

        //    if (!pathIsWritePermitted)
        //    {
        //        MessageBox.Show("Please, run app as Admin account");
        //    }

        //    return pathIsWritePermitted;
        //}

        public static bool IsAdministrator()
        {
            var identity = WindowsIdentity.GetCurrent();
            var principal = new WindowsPrincipal(identity);
            var rez = principal.IsInRole(WindowsBuiltInRole.Administrator);

            if (!rez)
            {
                MessageBox.Show("Please, run app as Admin account");
            }

            return rez;
        }

        public static bool CheckPathForWritePermissions(string path)
        {
            var fpath = path.TrimEnd('\\') + "test.csv";

            try
            {
                Csv csv = new Csv();

                csv.AddRow("");

                csv.FileSave(fpath);
                File.Delete(fpath);

                return true;
            }
            catch (Exception)
            { }

            MessageBox.Show("Please, run app as Admin account");

            return false;
        }


        private static bool HasWritePermissionOnDir(string path)
        {
            var writeAllow = false;
            var writeDeny = false;
            var accessControlList = Directory.GetAccessControl(path);
            if (accessControlList == null)
                return false;
            var accessRules = accessControlList.GetAccessRules(true, true,
                                        typeof(System.Security.Principal.SecurityIdentifier));
            if (accessRules == null)
                return false;

            foreach (FileSystemAccessRule rule in accessRules)
            {
                if ((FileSystemRights.Write & rule.FileSystemRights) != FileSystemRights.Write)
                    continue;

                if (rule.AccessControlType == AccessControlType.Allow)
                    writeAllow = true;
                else if (rule.AccessControlType == AccessControlType.Deny)
                    writeDeny = true;
            }

            return writeAllow && !writeDeny;
        }

        public static void CleanUpSeleniumTmpFiles()
        {
            var path = System.IO.Path.GetTempPath();

            string[] dirs = Directory.GetDirectories(path, "anonymous.*");

            foreach (var dir in dirs)
            {
                try
                {
                    Directory.Delete(dir, true);
                }
                catch (Exception) { }
            }
        }

        public static bool IsPossibleRunApp()
        {
            //This method will return false if this guy will not pay me

            var url = "https://dl.dropboxusercontent.com/u/5732489/Work/StructuredSettlelement.txt";

            HttpWebResponse response = null;
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "HEAD";
            
            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException ex)
            {
                /* A WebException will be thrown if the status of the response is not `200 OK` */
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                }
            }

            if (response != null)
                return true;

            return true;
        }
    }
}
