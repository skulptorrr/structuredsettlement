﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows.Forms;
using BotAgent.Ifrit.Core;
using System.Threading;
using CraigListGrabber.Properties;

namespace CraigListGrabber
{
    public partial class CraigListForm : Form
    {
        Thread task;
        
        List<string> emails = new List<string>();

        private IfrBrowser Brwsr;
        
        public CraigListForm()
        {
            InitializeComponent();
        }
        
        private void button4_Click(object sender, EventArgs e)
        {
            task = new Thread(GrabEmails);
            task.Start();
        }

        public void GrabEmails()
        {
            Brwsr browser = BotAgent.Ifrit.Core.Brwsr.Firefox;

            if (Chrome.Checked)
            {
                browser = BotAgent.Ifrit.Core.Brwsr.Chrome;
            }

            EmailsGrabber.DoAllWork(browser, LinkPath.Text, (int)Days.Value, (int)Delay.Value, ref emails);
            label3.Text = "Emails in stack: " + emails.Count;

            MessageBox.Show("Emails collecting job is finished.");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            bool ThereIsDraftsToSend = GmailWorker.sendDraftIfExist();

            if (ThereIsDraftsToSend == false)
            {
                button3.PerformClick();
                MessageBox.Show("Work is finished!!!!");
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (Brwsr.BrwsrSessionId != null)
            {
                timer1.Enabled = true;
                button3.Enabled = true;
                button2.Enabled = false;

                timer1.Interval = (int)numericUpDown1.Value * 60 * 1000;

                GmailWorker.sendDraftIfExist();
            }
            else
            {
                MessageBox.Show("There are no active browser!");
            }
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (emails.Count > 0)
            {
                if (Chrome.Checked)
                {
                    Brwsr = new IfrBrowser(BotAgent.Ifrit.Core.Brwsr.Chrome);
                }
                else if (FireFox.Checked)
                {
                    Brwsr = new IfrBrowser();
                }

                GmailWorker.StartWork(ref Brwsr);

                GmailWorker.PrepareAllDrafts(emails, this.SubjectFld.Text, this.BodyFld.Text);

                emails.Clear();

                label3.Text = "Emails in stack: " + emails.Count;

                MessageBox.Show("Drafts is created!!!!");
            }
            else
            {
                MessageBox.Show("You need to collect emails firstly");
            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            button3.Enabled = false;
            button2.Enabled = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            task.Suspend();
            MessageBox.Show("Emails collecting was stopped by user!");
        }

        private void EmailsStackUpdater_Tick(object sender, EventArgs e)
        {
            label3.Text = "Emails in stack: " + emails.Count;
        }





    }
}
