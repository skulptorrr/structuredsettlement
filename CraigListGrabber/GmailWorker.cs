﻿using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;
using BotAgent.Ifrit.Core;
using BotAgent.Ifrit.Core.BaseClasses;

namespace CraigListGrabber
{
    class GmailWorker
    {
        static private IfrBrowser Brwsr;

        public static void StartWork(ref IfrBrowser browser)
        {
            Brwsr = browser;
            
            Brwsr.Nav.GoTo("https://www.gmail.com");

            MessageBox.Show("Click OK when you will log in");

            Brwsr.Nav.GoTo("https://mail.google.com/mail/u/0/h/l8lhh4vee1i4/?zy=h&f=1");
        }

        public static void PrepareAllDrafts(List<string> emails, string subject, string body)
        {
            string emailsStr = string.Empty;

            for (int i = 0; i < emails.Count ; i++)
            {
                if (i % 3 == 0 && emailsStr!= string.Empty)
                {
                    PrepareSingleDraft(emailsStr, subject, body);
                    emailsStr = string.Empty;
                }
                emailsStr += emails[i] + ",";
            }
            if (emailsStr.Length > 3)
            {
                PrepareSingleDraft(emailsStr, subject, body);
            }
        }

        private static void PrepareSingleDraft(string emailsStr, string subject, string body)
        {
            emailsStr = emailsStr.TrimEnd(',');

            Brwsr.Page.Elem(By.Xpath(".//a[@accesskey='c']"),Await.Present(60000)).AsLink().ClickAndWaitForPageLoad();

            Brwsr.Page.Elem(By.Id("bcc")).AsInput().Text = emailsStr;

            Brwsr.Page.Elem(By.Name("subject")).AsTextArea().Text = subject;

            Brwsr.Page.Elem(By.Name("body")).AsTextArea().Text = body;

            Brwsr.Page.Elem(By.Xpath(".//input[@value='Save Draft']")).AsButton().ClickAndWaitForPageLoad();

            Thread.Sleep(1000);

        }

        public static bool sendDraftIfExist()
        {
            Brwsr.Page.Elem(By.Href("?&s=d")).AsLink().ClickAndWaitForPageLoad();
            
            var draftElem = Brwsr.Page.Elem(By.Xpath(".//form/table[2]/tbody/tr[1]/td[2]/font[text()='Draft']"), Await.Present(4000)).AsLink();

            bool draftExist = draftElem.IsExist();

            if (draftExist)
            {
                SendDraft();
                return true;
            }
            else
            {
                return false;
            }
        }

        private static void SendDraft()
        {
            Brwsr.Page.Elem(By.Xpath(".//form/table[2]/tbody/tr[1]/td[3]/a")).AsLink().ClickAndWaitForPageLoad();

            Brwsr.Page.Elem(By.Name("nvp_bu_send")).AsButton().ClickAndWaitForPageLoad();
        }



    }
}
