﻿using System;
using System.Windows.Forms;
using FileSystem;

namespace CraigListGrabber
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (FS.IsAdministrator())
            {
                FS.CleanUpSeleniumTmpFiles();

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new CraigListForm());
            }
        }
    }
}
