﻿using System;
using System.Collections.Generic;
using System.Threading;
using BotAgent.Ifrit.Core;
using BotAgent.Ifrit.Core.BaseClasses;

namespace CraigListGrabber
{
    class EmailsGrabber
    {
        static IfrBrowser Brwsr;

        public static void DoAllWork(BotAgent.Ifrit.Core.Brwsr browser, string path, int perDays, int delay, ref List<string> emails)
        {
            Brwsr = new IfrBrowser(browser);

            var linksList = GetLinksList(path, perDays);

            GetEmails(linksList, delay, ref emails);

            Brwsr.Close();
        }

        private static List<string> GetLinksList(string path, int perDays)
        {
            Brwsr.Nav.GoTo(path);

            string correctDateRange = "";

            for (int i = 0; i <= perDays; i++)
            {
                var dayToStartFromDt = DateTime.Today.AddDays(-i);
                correctDateRange += String.Format("contains(@datetime,'{0}-{1}-{2}') or ", dayToStartFromDt.Year, dayToStartFromDt.Month.ToString("d2"), dayToStartFromDt.Day.ToString("d2"));
            }
            correctDateRange = correctDateRange.Substring(0, correctDateRange.Length - 4);

            var xpath = ".//time[@class='result-date'][" + correctDateRange + "]//../a";

            int linksToParse = Brwsr.Page.Elems(By.Xpath(xpath)).AsList().Count;

            var hrefs = Brwsr.Page.Elems(By.Xpath(xpath)).AsListOfHrefs();
            
            return hrefs;
        }

        private static string GetEmailFromPage(string path, int delay)
        {
            Brwsr.Nav.GoTo(path);

            Brwsr.Page.Elem(By.Class("reply_button")).AsButton().Click();

            while (Brwsr.Page.Elem(By.Id("g-recaptcha")).AsOther().IsVisible())
            {
                Thread.Sleep(1000);
            }

            Thread.Sleep(delay*1000);

            var email = Brwsr.Page.Elem(By.Class("anonemail")).AsOther().Text;
            
            return email;
        }

        private static void GetEmails(List<string> pagesList, int delay, ref List<string> emails)
        {
            foreach (var pageLink in pagesList)
            {
                var email = GetEmailFromPage(pageLink, delay);
                if (email != "")
                {
                    emails.Add(email);
                }
            }
        }
    }
}
