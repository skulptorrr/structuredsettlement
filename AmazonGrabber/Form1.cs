﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;
using AmazonGrabber.Properties;
using BotAgent.DataExporter;
using BotAgent.Ifrit.Core;
using BotAgent.Ifrit.Core.BaseClasses;
using BotAgent.Ifrit.Extensions;

namespace AmazonGrabber
{
    public partial class Form1 : Form
    {
        private List<string> _bestSellersPages = new List<string>();
        private List<string> _bestSellersProductsLinks = new List<string>();
        private Csv _csvFile = new Csv();
        private Thread _workerThread;

        private bool _runningState =  true;

        private static IfrBrowser Brwsr;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _workerThread = new Thread(DoWork);
            _workerThread.Start();
        }

        private void DoWork()
        {
            StartWork();
            
            AddRow1ToCsv();

            CollectBestSellersPagesLinks();
            CollectAllBestSellersProducts();

            for (int i = 0; i < _bestSellersProductsLinks.Count; i++)
            {
                Brwsr.Nav.GoTo(_bestSellersProductsLinks[i]);
                CollectCollectProductInfo();
            }

            var counter = 0;

            foreach (var link in _bestSellersProductsLinks)
            {
                counter++;

                if (counter % 5 == 0)
                {
                    SaveCsvToHdd();
                }

                Brwsr.Nav.GoTo(link);
                CollectCollectProductInfo();

                Thread.Sleep(Settings.Default.WaitPeriod * 1000 );
            }

            EndWork();
        }



        private void AddRow1ToCsv()
        {
            List<string> row = new List<string> { 
                "Item Name", "Item model number", 
                "New Price 1", "New Price 2", "New Price 3", "New Price Average", "", 
                "Used Price 1", "Used Price 2", "Used Price 3", "Used Price Average", "", 
                "Refurbished Price 1", "Refurbished Price 2", "Refurbished Price 3", "Refurbished Price Average",""
                //"Used/new Price 1", "Used/new Price 2", "Used/new Price 3", "Used/new Price Average"
            };

            _csvFile.Rows.Add(row);
        }

        private void AddRowToCsv(string title, string modelNum, double newPrice1, double newPrice2, double newPrice3, double usedPrice1, double usedPrice2, double usedPrice3, double refurbishedPrice1, double refurbishedPrice2, double refurbishedPrice3, double usedOrNew1, double usedOrNew2, double usedOrNew3)
        {
            double newPriceAverage = GetCorrectAverage(newPrice1, newPrice2, newPrice3);

            double usedPriceAverage = GetCorrectAverage(usedPrice1, usedPrice2, usedPrice3);

            double refurbishedPriceAverage = GetCorrectAverage(refurbishedPrice1, refurbishedPrice2, refurbishedPrice3);

            double usedOrNewPriceAverage = GetCorrectAverage(usedOrNew1, usedOrNew2, usedOrNew3);

            List<string> row = new List<string>() { title, modelNum,
                ToDecStr(newPrice1), ToDecStr(newPrice2), ToDecStr(newPrice3), ToDecStr(newPriceAverage), "",
                ToDecStr(usedPrice1), ToDecStr(usedPrice2), ToDecStr(usedPrice3), ToDecStr(usedPriceAverage), "",
                ToDecStr(refurbishedPrice1), ToDecStr(refurbishedPrice2), ToDecStr(refurbishedPrice3), ToDecStr(refurbishedPriceAverage),""
                //ToDecStr(usedOrNew1), ToDecStr(usedOrNew2), ToDecStr(usedOrNew3), ToDecStr(usedOrNewPriceAverage)
            };

            _csvFile.Rows.Add(row);
        }

        private string ToDecStr(double number)
        {
            return Convert.ToInt32(number).ToString();
        }


        private void SaveCsvToHdd()
        {
            _csvFile.FileSave(Settings.Default.PathToSaveRezults.TrimEnd('\\') + "\\" + Settings.Default.RezultFileName + ".csv");
        }

        private double GetCorrectAverage(double n1, double n2, double n3)
        {
            double summ = n1 + n1 + n3;

            if (n2 == 0)
            {
                return summ;
            }
            if (n3 == 0)
            {
                return Math.Round( (summ / 2) , 2); 
            }

            return Math.Round((summ / 3));
        }

        private void StartWork()
        {
            //// HideMainForm
            //this.WindowState = FormWindowState.Minimized;
            //this.ShowInTaskbar = false;

            Brwsr = new IfrBrowser();

            this.button1.Enabled = true;
        }

        private void EndWork()
        {
            SaveCsvToHdd();

            Brwsr.Close();

            Thread.Sleep(4000);

            Close();
        }

        private void CollectBestSellersPagesLinks()
        {
            _bestSellersPages.Add(Settings.Default.Url);

            Brwsr.Nav.GoTo(Settings.Default.Url);

            for (int i = 2; i <= 5; i++)
            {
                var href = Brwsr.Page.Elem(By.Xpath(".//li[@id='zg_page"+i+"']/a")).AsLink().GetAttribute("href");
                _bestSellersPages.Add(href);
            }
        }

        private void CollectAllBestSellersProducts()
        {
            foreach (var link in _bestSellersPages)
            {
                CollectAllBestSellersProductsLinksFromPage(link);
            }
        }

        private void CollectAllBestSellersProductsLinksFromPage(string pageUrl)
        {
            Brwsr.Nav.GoTo(pageUrl);

            var hrefsList = Brwsr.Page.Elems(By.Xpath("id('zg_centerListWrapper')/div/div/div/a")).AsListOfHrefs();

            _bestSellersProductsLinks.AddRange(hrefsList);
        }


        private void CollectCollectProductInfo()
        {
            var title = GetProductTitle();
            var model = GetItemModelNumber();

            double[] prices = GetPrices();

            AddRowToCsv(title, model, prices[0], prices[1], prices[2], prices[3], prices[4], prices[5], prices[6], prices[7], prices[8], prices[9], prices[10], prices[11]);
        }

        /// <summary>
        /// 0-2 = new
        /// 3-5 = used
        /// 6-8 = refurbished
        /// 9-12 = used or new
        /// </summary>
        private double[] GetPrices()
        {
            double[] prices = {0.0,0,0,0,0,0,0,0,0,0,0,0,0};
            double[] pricesTmp;
            string usedLink = string.Empty;
            string refurbishedLink = string.Empty;
            string newLink = string.Empty;
            string usedAndNewLink = string.Empty;

            newLink = Brwsr.Page.Elem(By.Xpath(".//a[contains(@href,'dp_olp_new')]")).AsLink().Href;
            usedLink = Brwsr.Page.Elem(By.Xpath(".//a[contains(@href,'dp_olp_used')]")).AsLink().Href;
            refurbishedLink = Brwsr.Page.Elem(By.Xpath(".//a[contains(@href,'dp_olp_refurbished')]")).AsLink().Href;
            usedAndNewLink = Brwsr.Page.Elem(By.Xpath(".//a/b[contains(text(), 'Used & new')]/..")).AsLink().Href;

            //New Link
            Brwsr.Nav.GoTo(newLink);
            pricesTmp = GetPricesFromPage();

            if (pricesTmp.Length > 0 && (pricesTmp[0] == 0))
            {
                Brwsr.Nav.GoTo(usedAndNewLink);
                    
                var NewCheckbox =  Brwsr.Page.Elem(By.Xpath(".//span/span[contains(text(),'New')][not(contains(text(),'Like'))]/../../input")).AsOther();
                if (NewCheckbox.IsExist())
                {
                    NewCheckbox.Click();
                    pricesTmp = GetPricesFromPage();
                }

                prices[0] = pricesTmp[0];
                prices[1] = pricesTmp[1];
                prices[2] = pricesTmp[2];
            }

            //Used
            Brwsr.Nav.GoTo(usedLink);
            pricesTmp = GetPricesFromPage();
            
            if (pricesTmp.Length > 0 && (pricesTmp[0] == 0))
            {
                Brwsr.Nav.GoTo(usedAndNewLink);

                var UsedCheckbox = Brwsr.Page.Elem(By.Xpath(".//span/span[contains(text(),'Used')]/../../input")).AsOther();
                if (UsedCheckbox.IsExist())
                {
                    UsedCheckbox.Click();
                    pricesTmp = GetPricesFromPage();
                }
            }

            prices[3] = pricesTmp[0];
            prices[4] = pricesTmp[1];
            prices[5] = pricesTmp[2];

            //Refurbished
            Brwsr.Nav.GoTo(refurbishedLink);
            pricesTmp = GetPricesFromPage();

            if (pricesTmp.Length > 0 && (pricesTmp[0] == 0))
            {
                Brwsr.Nav.GoTo(usedAndNewLink);

                var RefurbishedCheckbox = Brwsr.Page.Elem(By.Xpath(".//span/span[contains(text(),'Refurbished')]/../../input")).AsOther();
                if (RefurbishedCheckbox.IsExist())
                {
                    RefurbishedCheckbox.Click();
                    pricesTmp = GetPricesFromPage();
                }
            }

            prices[6] = pricesTmp[0];
            prices[7] = pricesTmp[1];
            prices[8] = pricesTmp[2];
            

            //if (usedAndNewLink != string.Empty)
            //{
            //    Brwsr.Nav.GoTo(usedAndNewLink);
            //    pricesTmp = GetPricesFromPage();
            //    prices[9] = pricesTmp[0];
            //    prices[10] = pricesTmp[1];
            //    prices[11] = pricesTmp[2];
            //}

            return prices;
        }

        /// <summary>
        /// 0-2 = new
        /// 3-5 = used
        /// 6-8 = refurbished
        /// 9-12 = used or new
        /// </summary>
        private void DoubleCheckParsing(ref double[] parsedPrices)
        {
        }


        private double[] GetPricesFromPage()
        {
            List<double> pricesOnThePage = new List<double>();

            var pricesElemsList = Brwsr.Page.Elems(By.Xpath(".//span[@class='a-size-large a-color-price olpOfferPrice a-text-bold']")).AsList();

            foreach (var priceElement in pricesElemsList)
            {
                if (pricesOnThePage.Count > 2)
                {
                    return pricesOnThePage.ToArray();
                }

                var priceStr = priceElement.AsOther().Text.Trim('$', ' ').Replace(",", string.Empty).Replace(".", ",");

                double price = priceStr.DoubleParseAdvanced();

                pricesOnThePage.Add(price);
            }

            for (int i = 1; i < 4; i++)
            {
                pricesOnThePage.Add(0);
            }

            return pricesOnThePage.ToArray();
        }


        private string GetItemModelNumber()
        {
            return Brwsr.Page.Elem(By.Xpath(".//tr['item-model-number']/td[@class='label' and text()='Item model number']/../td[@class='value']"), Await.Visible(500)).AsOther().Text;
        }

        private string GetProductTitle()
        {
            return Brwsr.Page.Elem(By.Id("productTitle")).AsOther().Text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (_runningState == true)
            {
                _workerThread.Suspend();
                SaveCsvToHdd();
                button1.Text = "Resume";
                _runningState = false;
            }
            else
            {
                _workerThread.Resume();
                button1.Text = "Pause";
                _runningState = true;
            }
        }

    }
}
